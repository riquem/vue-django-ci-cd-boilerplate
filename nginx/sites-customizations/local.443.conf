server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name NGINX_SERVER_NAME;

    #ssl_certificate /etc/letsencrypt/live/NGINX_SERVER_NAME/fullchain.pem; # Activate this if you want secure ssl internally
    #ssl_certificate_key /etc/letsencrypt/live/NGINX_SERVER_NAME/privkey.pem; # Activate this if you want secure ssl internally
    ssl_certificate /etc/ssl/certs/ssl-cert-snakeoil.pem;
    ssl_certificate_key /etc/ssl/certs/ssl-cert-snakeoil.key;

    ssl_session_cache shared:SSL:20m;
    ssl_session_timeout 60m;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;

    location = /robots.txt {
      root /crawlers/;
    }

    location = /sitemap.xml {
        root /crawlers/;
    }

    location / {
        # everything is passed to Gunicorn
        proxy_pass http://app_server;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_redirect off;
        client_max_body_size 50M; # allow up to 50MB user-provided file-upload size.
    }

    location NGINX_IMAGE_STATICFILES_DESTINATION_DIRECTORY/ {
        root /;
    }

    location NGINX_IMAGE_MEDIAFILES_DESTINATION_DIRECTORY/ {
        root /;
    }

    location /gitlab-badges/ {
        root /;
    }

    gzip on;
    gzip_vary on;
    gzip_proxied expired no-cache no-store private auth;
    gzip_types text/plain text/css text/javascript text/xml image/gif image/jpeg image/png image/svg+xml image/x-icon application/javascript application/xml;
    gzip_min_length 10240;
    gzip_disable "MSIE [1-6]\.";
    gunzip on;
}
