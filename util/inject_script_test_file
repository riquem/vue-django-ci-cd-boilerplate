// For example we want to replace some sections of an nginx config file

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name INJECT_SERVER_NAME;

    ssl_certificate /etc/letsencrypt/live/INJECT_SERVER_NAME/fullchain.pem;
    ssl_certificate_key INJECT_PATH_TO_PRIVATE_KEY/privkey.pem;

    ssl_session_cache shared:SSL:20m;
    ssl_session_timeout 60m;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;

    location / {
        # everything is passed to Gunicorn
        proxy_pass http://app_server;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_redirect off;
        client_max_body_size 50M; # allow up to 50MB user-provided file-upload size.
    }

    location INJECT_PATH_TO_STATICFILES {
        root /;
    }

    location INJECT_PATH_TO_MEDIA_FILES {
        root /;
    }
}
